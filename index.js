var http = require('http');
var fs = require('fs');
var util = require('util');
var formidable = require('formidable');
var os = require('os');

http.createServer(function(req, res) {
  if (req.method.toLowerCase() == 'post') {
    // parse a file upload 
    var form = new formidable.IncomingForm();
    form.multiples = true;
 
    form.parse(req, function(err, fields, files) {
      res.writeHead(200, {'content-type': 'text/plain'});
      res.write('received upload:\n\n');
      res.end(util.inspect({fields: fields, files: files}));;

    });
    form.on('file', function(name, file){
        console.log(file.name);
        fs.readFile(file.name, function(err, data){
            var sender = data.toString('utf8').substring(data.toString('utf8').indexOf('From:'),data.toString('utf8').indexOf('>'));
            fs.writeFile('output.txt', sender);
            //console.log(data.toString('utf8').indexOf('From:'),data.toString('utf8').indexOf('From:'), data.toString('utf8').charAt(1582));
        });
    });
 
    return;
  }

 
  // show a file upload form 
  res.writeHead(200, {'content-type': 'text/html'});
  res.end(
    '<form action="/upload" enctype="multipart/form-data" method="post">'+
    '<input type="file" name="upload" multiple><br>'+ 
    '<input type="submit" value="Upload">'+
    '</form>'
  );
}).listen(8080);